import multiprocessing
import sys
import os

def inputer(qqinput, sse, n):
    sys.stdin = os.fdopen(sse)   #перенастраиваем ввод
    while True:
        a = input()
        if a == 'exit':   #Если на входе exit то закидываем None-ы по количеству рабочих процессов
            for i in range(n):
                qqinput.put(None)
            break
        x = [int(a[0]),a[1],int(a[2])]
        qqinput.put(x)

def outputer(qqoutput, n):
    a = 0  #cчетчих завершенных worker-ов
    while True:
        f = qqoutput.get()
        if f == None:
            a += 1
        else:
            print(f)
        if(a == n):   #после завершения всез worker-ов завершаем процесс
            break

def worker(qqinput, qqoutput):
    while True:
        d = qqinput.get()
        if d == None:   #Если на входе None то завершаем процесс и закидываем на выход None
            qqoutput.put(None)
            break
        elif d[1]=='+':
            qqoutput.put(d[0]+d[2])
        elif d[1]=='-':
            qqoutput.put(d[0]-d[2])

if __name__ =='__main__':
    n = 1  #кол-во процессов worker
    qinput = multiprocessing.Queue() #очередь входа и выхода
    qoutput = multiprocessing.Queue()
    fn = sys.stdin.fileno()

    outputer1 = multiprocessing.Process(target=outputer, args=(qoutput, n))  #создаем процессы
    inputer1 = multiprocessing.Process(target=inputer, args=(qinput, fn, n))
    Threads = []
    for i in range(n):
        thread = multiprocessing.Process(target=worker,args =(qinput, qoutput))
        Threads.append(thread)

    inputer1.start()   #запускаем процессы
    outputer1.start()
    for i in Threads:
        i.start()

    inputer1.join()    #ждем завершения процессов
    outputer1.join()
    for i in Threads:
        i.join()
#1+3
#4-5
#exit