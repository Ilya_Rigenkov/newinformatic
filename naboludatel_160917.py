class Subject():
    def register_observer(self, o):
        raise NotImplementedError()
    def remove_observer(self, o):
        raise NotImplementedError()
    def notify_observers(self):
        raise NotImplementedError()

class Observer():
    def update(self, temperature, humidity, pressure):
        raise NotImplementedError()

class DisplayElement():
    def display(self):
        raise NotImplementedError()

class WeatherData(Subject):
    def __init__(self):
        self.observers = []
        self.temperature = None
        self.humidity = None
        self.pressure = None
    def register_observer(self, o):
        if isinstance(o, Observer):
            self.observers.append(o)
        else:
            raise TypeError('{0} not type Observer'.format(type(o)))
    def remove_observer(self, o):
        if o not in self.observers:
            raise ImportError('{0} is not Observer now'.format(o))
        else:
            if isinstance(o, Observer):
                self.observers.remove(o)
            else:
                raise TypeError('{0} not type Observer'.format(type(o)))
    def notify_observers(self):
        for i in range(len(self.observers)):
            self.observers[i].update(self.temperature, self.humidity, self.pressure)
    def measurementsChanged(self):
        self.notify_observers()
    def set_measurements(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.measurementsChanged()
    def get_temperature(self):
        return self.temperature
    def get_humidity(self):
        return self.humidity
    def get_pressure(self):
        return self.pressure

class CurrentConditionsDisplay(Observer, DisplayElement):
    def __init__(self):
        self.temperature = None
        self.pressure = None
        self.humidity = None
        self.weatherData = WeatherData()
        self.weatherData.register_observer(self)
    def update(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.display()
    def display(self):
        print('Current conditions: {0} F degrees and {1}% of humidity. Pressure is {2} Pa'.format(self.temperature, self.humidity, self.pressure))


if __name__ == '__main__':
    weather_data = WeatherData()
    current_conditions = CurrentConditionsDisplay(weather_data)

    weather_data.set_measurements(80, 65, 34555)
    weather_data.set_measurements(67,80,40000)
    weather_data.set_measurements(40,50,70000)