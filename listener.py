import socket
server = socket.socket()
server.connect(('localhost', 9090))

while True:
    raw_data = server.recv(1024)
    data = raw_data.decode('utf-8')
    if data == 'exit':
        print(data)
        break
    print(data)
server.close()