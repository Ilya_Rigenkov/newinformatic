class Beverage():
    def __init__(self):
        self.description = 'Unknown Beverage'

    def getDescription(self):
        return self.description
    def setSize(self, size):
        self.size = size
    def getSize(self):
        return self.size
    def cost(self):
        raise NotImplementedError

class CondimentDecorator(Beverage):
    def __init__(self):
        self.beverage = None
    def getSize(self):
        return self.beverage.getSize()
    def getDescription(self):
        raise NotImplementedError

class DarkRoast(Beverage):
    def __init__(self):
        self.description = 'Dark Roast Coffee'
    def cost(self):
        return 0.99

class Decaf(Beverage):
    def __init__(self):
        self.description = 'Decaf Coffee'
    def cost(self):
        return 1.05

class Espresso(Beverage):
    def __init__(self):
        self.description = "Espresso"
    def cost(self):
        return 1.99

class HouseBlend(Beverage):
    def __init__(self):
        self.description = "House Blend Coffee"
    def cost(self):
        return 0.89

class Milk(CondimentDecorator):
    def __init__(self, beverage):
        self.beverage = beverage
    def getDescription(self):
        return self.beverage.getDescription()+ ', Milk'
    def cost(self):
        return self.beverage.cost() + 0.10

class Mocha(CondimentDecorator):
    def __init__(self, beverage):
        self.beverage = beverage
    def getDescription(self):
        return self.beverage.getDescription() + ', Mocha'
    def cost(self):
        return self.beverage.cost() + 0.20
class Soy(CondimentDecorator):
    def __init__(self, beverage):
        self.beverage = beverage
    def getDescription(self):
        return self.beverage.getDescription() + ', Soy'
    def cost(self):
        self.cost = self.beverage.cost()
        if (self.beverage.getSize() == 'TALL'):
            self.cost += 0.10
        elif (self.beverage.getSize() == 'GRANDE'):
            self.cost += 0.15
        elif (self.beverage.getSize() == 'VENTI'):
            self.cost += 0.20
        return self.cost
class Whip(CondimentDecorator):
    def __init__(self, beverage):
        self.beverage = beverage
    def getDescription(self):
        return self.beverage.getDescription() + ', Whip'
    def cost(self):
        return self.beverage.cost() + 0.10
class StarbuzzCoffee():
    def __init__(self):
        self.beverage = Espresso()
        print(self.beverage.getDescription()+" "+str(self.beverage.cost()))
        self.beverage2 = DarkRoast()
        self.beverage2 = Mocha(self.beverage2)
        self.beverage2 = Mocha(self.beverage2)
        self.beverage2 = Whip(self.beverage2)
        print(self.beverage2.getDescription()+' '+str(self.beverage2.cost()))
        self.beverage3 = HouseBlend()
        self.beverage3.setSize('VENTI')
        self.beverage3 = Soy(self.beverage3)
        self.beverage3 = Mocha(self.beverage3)
        self.beverage3 = Whip(self.beverage3)
        print(self.beverage3.getDescription() +' '+ str(self.beverage3.cost()))
A = StarbuzzCoffee()