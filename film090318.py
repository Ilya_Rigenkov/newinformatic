import requests as r
from bs4 import BeautifulSoup as Bs
import pickle
import os

s1 = "https://www.kinopoisk.ru/top/navigator/m_act[year]/2018/order/rating/perpage/200/#results"

if os.path.exists("html.pickle"):
    with open("html.pickle", "rb") as f:
        content = pickle.load(f)
else:
    page = r.get(s1)
    if page.status_code == 200:
        content = page.content.decode("cp1251")
        # print(content)

        with open("html.pickle", 'wb') as f:
            pickle.dump(content, f)
    else:
        print(page.status_code)

h = Bs(content, 'html.parser')
Films = []
for el in h.find_all(attrs={'id': "itemList"}):
    for div in el.find_all(attrs={'class': "name"}):
        #print(div.a.text, div.a.get('href'))
        Films.append('https://www.kinopoisk.ru' + div.a.get('href') + 'cast/')
page = r.get(Films[0])
content = page.content.decode("cp1251")
rawactors = Bs(content, 'html.parser')
for el in rawactors.find_all(attrs ={'class': "dub"}):
    for div in el.find_all(attrs={'class': "name"}):
        print(div.a.text, div.a.get('href'))


