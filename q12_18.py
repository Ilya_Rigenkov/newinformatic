import threading
import queue

qinput = queue.Queue()  #создаем очередь входа(inputer to worker) и выхода(worker to outputer)
qoutput = queue.Queue()
n = 4   #кол-во рабочих потоков

def inputer():
    while True:
        a = input()
        if a == 'exit':   #Если на входе exit то закидываем None-ы по количеству рабочих потоков
            for i in range(n):
                qinput.put(None)
            break
        x = [int(a[0]),a[1],int(a[2])]
        qinput.put(x)

def outputer():
    a = 0  #cчетчих завершенных worker-ов
    while True:
        f = qoutput.get()
        if f == None:
            a += 1
        else:
            print(f)
        if(a == n):  #после завершения всез worker-ов завершаем поток
            break

def worker():
    while True:
        d = qinput.get()
        if d == None:       #Если на входе None то завершаем поток и закидываем на выход None
            qoutput.put(None)
            break
        elif d[1]=='+':
            qoutput.put(d[0]+d[2])
        elif d[1]=='-':
            qoutput.put(d[0]-d[2])

inputer1 = threading.Thread(target=inputer)     #создаем потоки
outputer1 = threading.Thread(target = outputer)
Threads = []
for i in range(n):
    thread = threading.Thread(target=worker)
    Threads.append(thread)

inputer1.start()   #запускаем потоки
outputer1.start()
for i in Threads:
    i.start()

inputer1.join()  #ждем завершения потоков
outputer1.join()
for i in Threads:
    i.join()
