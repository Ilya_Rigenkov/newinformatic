import socket

sock = socket.socket()
sock.bind(('', 9090))
sock.listen(2)
speaker, addr1 = sock.accept()
listener, addr2 = sock.accept()

print('connected:', addr1)
print('connected:', addr2)

while True:
    raw_data = speaker.recv(1024)
    if raw_data.decode('utf-8') == 'exit':
        listener.send(raw_data)
        break
    listener.send(raw_data)

speaker.close()
listener.close()