A = input()
Symb = {}
for i in range(len(A)):
    Symb.setdefault(A[i],0)
    Symb[A[i]] = Symb[A[i]] + 1
minNum = min(Symb.values())
Min = []
L = ''
for i in Symb:
    if Symb[i] == minNum:
        Min.append([i,Symb[i]])
    else:
        L = L + (i * Symb[i])
if len(Min)==1:
    print(1)
    print(A)
else:
    if len(Min)==len(A):
        print(len(A))
        for i in range(len(A)):
            print(A[i])
    else:
        print(2)
        for i in range(len(Min)-1):
             L = L+Min[i][0]*Min[i][1]
        L = L + (Min[len(Min)-1][0]*(Min[len(Min)-1][1]-1))
        print(Min[len(Min)-1][0])
        print(L)
