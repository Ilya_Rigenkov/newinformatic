import socket

sock = socket.socket()
sock.bind(('', 9090))
sock.listen(1)
conn, addr = sock.accept()

print('connected:', addr)

while True:
    data = conn.recv(1024)
    sdata = data.decode('utf-8')
    if sdata=='exit':
        conn.send('exit'.encode('utf-8'))
        break
    sdata = sdata.upper()
    output_data = sdata.encode('utf-8')
    conn.send(output_data)

conn.close()