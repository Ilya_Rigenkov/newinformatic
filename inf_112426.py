A = input().rpartition('.')[0]
A = A.upper()
lis = {}
for i in range(len(A)):
    if A[i] not in lis:
        lis.setdefault(A[i],1)
    else:
        lis[A[i]] += 1

lis.pop(' ',None)
Max = [0,None]
for i in lis:
    if lis[i]>Max[0]:
        Max[1] = i
        Max[0] = lis[i]
    if lis[i] == Max[0] and i<Max[1]:
        Max[1] = i

print(Max[1],Max[0])