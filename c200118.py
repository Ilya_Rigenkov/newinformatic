import socket
sock = socket.socket()
sock.connect(('localhost', 9090))

while True:
    a = input()
    data = a.encode('utf-8')
    sock.send(data)

    res_data = sock.recv(1024)
    ans = res_data.decode('utf-8')
    if ans=='exit':
        break
    print(ans)
sock.close()