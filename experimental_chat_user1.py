import socket
import threading

id = 'Ilya'
server_input = socket.socket()
server_input.connect(('localhost', 9090))
server_output = socket.socket()
server_output.connect(('localhost', 9090))
server_input.send(id.encode('utf-8'))

def speaker():
    while True:
        data = input()
        if data == 'exit':
            server_input.send(data.encode('utf-8'))
            break
        data = id + '#' + data
        server_input.send(data.encode('utf-8'))

def listener():
    while True:
        raw_data = server_output.recv(1024)
        data = raw_data.decode('utf-8')
        if data == 'exit':
            print(data)
            break
        else:
            print(data[data.find('#')+1::])

speak = threading.Thread(target = speaker)
listen = threading.Thread(target = listener)

speak.start()
listen.start()

speak.join()
listen.join()

server_output.close()
server_input.close()