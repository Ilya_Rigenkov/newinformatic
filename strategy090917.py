class MiniDuckSimulator():
    def __init__(self):
        self.mallard = MallardDuck()
        self.rubberDuckie = RubberDuck()
        self.decoy = DecoyDuck()
        self.model = ModelDuck()

        self.mallard.performQuack()
        self.rubberDuckie.performQuack()
        self.decoy.performQuack()
        self.model.performFly()
        self.model.setFlyBehavior(FlyRocketPowered())
        self.model.performFly()


class Duck():
    def __init__(self):
        self.FlyBehavior = None
        self.QuackBehavior = None
    def setFlyBehavior(self, fb):
        self.FlyBehavior = fb
    def setQuackBehavior(self, qb):
        self.QuackBehavior = qb
    def display(self):
        raise NotImplementedError()
    def performFly(self):
        self.FlyBehavior.fly()
    def performQuack(self):
        self.QuackBehavior.quack()
    def swim(self):
        print("All ducks float, even decoys!")


class QuackBehavior():
    def quack(self):
        raise NotImplementedError()
class FlyBehavior():
    def fly(self):
        raise NotImplementedError()


class FlyWithWings(FlyBehavior):
    def fly(self):
        print("I'm flying!!")
class FlyNoWay(FlyBehavior):
    def fly(self):
        print("I can't fly")
class FlyRocketPowered(FlyBehavior):
    def fly(self):
        print("I'm flying with a rocket")


class Quack(QuackBehavior):
    def quack(self):
        print("Quack")
class Squeak(QuackBehavior):
    def quack(self):
        print("Squeak")
class MuteQuack(QuackBehavior):
    def quack(self):
        print("<< Silence >>")


class MallardDuck(Duck):
    def __init__(self):
        self.FlyBehavior = FlyWithWings()
        self.QuackBehavior = Quack()
    def display(self):
        print("I'm a real Mallard duck")
class RedHeadDuck(Duck):
    def __init__(self):
        self.FlyBehavior = FlyWithWings()
        self.QuackBehavior = Quack()
    def display(self):
        print("I'm a real Red Headed duck")
class RubberDuck(Duck):
    def __init__(self):
        self.FlyBehavior = FlyNoWay()
        self.QuackBehavior = Squeak()
    def display(self):
        print("I'm a rubber duckie")
class DecoyDuck(Duck):
    def __init__(self):
        self.FlyBehavior = FlyNoWay()
        self.QuackBehavior = MuteQuack()
    def display(self):
        print("I'm a duck Decoy")
class ModelDuck(Duck):
    def __init__(self):
        self.FlyBehavior = FlyNoWay()
        self.QuackBehavior = Quack()
    def display(self):
        print("I'm a model duck")


if __name__ == '__main__':
    a = MiniDuckSimulator()