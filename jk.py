from collections import defaultdict
t = input().upper().split('.')[0]
d = {}
for el in t:
    if el != ' ':
        if el not in d:
            d[el] = 1
        else:
            d[el] += 1
m = sorted([(v,k) for k,v in d.items()], reverse=1)
c1,s1 = m[0]
i = 0
for c1,s1 in m[1:]:
    if c1 != m[i][0]:
        print(m[i][1],m[i][0])
        break
    else:
        i += 1
