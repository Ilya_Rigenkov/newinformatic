import socket
import threading

sock = socket.socket()
sock.bind(('', 9090))
sock.listen(4)

speaker1, addr1 = sock.accept()
listener1, addr2 = sock.accept()
id1 = speaker1.recv(1024).decode('utf-8')
speaker2, addr3 = sock.accept()
listener2, addr4 = sock.accept()
id2 = speaker2.recv(1024).decode('utf-8')

def speak1():
    while True:
        raw_data = speaker1.recv(1024)
        data = raw_data.decode('utf-8')
        if data == 'exit':
            listener1.send(raw_data)
            listener2.send(raw_data)
            break
        listener2.send(raw_data)

def speak2():
    while True:
        raw_data = speaker2.recv(1024)
        data = raw_data.decode('utf-8')
        if data == 'exit':
            listener1.send(raw_data)
            listener2.send(raw_data)
            break
        listener1.send(raw_data)

sp1 = threading.Thread(target = speak1)
sp2 = threading.Thread(target = speak2)

sp1.start()
sp2.start()

sp1.join()
sp2.join()

speaker1.close()
listener1.close()
speaker2.close()
listener2.close()