import multiprocessing
import sys
import os

def inputer(qqinput):
    file = open('task.txt')  #открываем файл с заданием
    raw_task = file.read()
    file.close()
    i = 0
    while i < len(raw_task):
        qqinput.put(int(raw_task[i:i+4]))
        i = i+4
    qqinput.put(None)   #после конца файла закидываем None
def outputer(qqoutput, n):  #аналогично классной работе
    a = 0
    while True:
        f = qqoutput.get()
        if f == None:
            a += 1
        else:
            print(f)
        if(a == n):
            break

def worker(qqinput, qqoutput):  #аналогично классной работе, но считает факториал
    while True:
        d = qqinput.get()
        if d == None:
            qqoutput.put(None)
            break
        cout = 1
        ad =1
        while ad < d:
            cout = cout*ad
            ad += 1
        qqoutput.put(cout)
#далее всё аналогично классной работе
if __name__ =='__main__':
    n = 4
    qinput = multiprocessing.Queue()
    qoutput = multiprocessing.Queue()
    fn = sys.stdin.fileno()

    outputer1 = multiprocessing.Process(target=outputer, args=(qoutput, n))
    inputer1 = multiprocessing.Process(target=inputer, args=(qinput,))
    Threads = []
    for i in range(n):
        thread = multiprocessing.Process(target=worker,args =(qinput, qoutput))
        Threads.append(thread)

    inputer1.start()
    outputer1.start()
    for i in Threads:
        i.start()

    inputer1.join()
    outputer1.join()
    for i in Threads:
        i.join()