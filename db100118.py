import sqlite3
conn = sqlite3.connect('Products.db')
curs = conn.cursor()

def PrintTable(c,reqest):
    for raw in c.execute(reqest):
        print(raw)

drop_table_product = '''
DROP TABLE IF EXISTS product;
'''
drop_table_customer = '''
DROP TABLE IF EXISTS customer;
'''
drop_table_purchase = '''
DROP TABLE IF EXISTS purchase
'''
create_table_product = '''
CREATE TABLE product(
`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
`name` TEXT,
`price` FLOAT
);
'''
create_table_customer = '''
CREATE TABLE customer(
`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
`fio` TEXT,
`adress` TEXT
);
'''
create_table_purchase = '''
CREATE TABLE purchase(
`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
`time` TEXT,
`id_customer` INTEGER,
`id_product` INTEGER,
`count` INTEGER
);
'''
list_table_name = '''
SELECT name FROM sqlite_master WHERE type='table';
'''


values_product = [
    ('iphone X','ipad pro','iphone 9','macbook pro','ice-cream'),
    (80000,40000,30000,120000,10)
]
product_columns = ('name','price')

values_customers = [
    ('Иванов Иван Иванович','Александров Александр Александрович','Петров Петр Петрович'),
    ('г.Екатеринбург ул. Бажова д. 5 кв 7','г.Асбест ул. Парижская 89 кв 98','г.Екатеринбург ул. Ленина д. 1 кв 1')
]
customer_columns = ('fio','adress')

values_purchase = [
    ('2018-01-01 22:12:01','2018-01-01 22:59:59','2018-01-02 07:00:01','2018-02-01 12:30:43','2017-12-30 16:23:04','2017-02-03 19:04:39'),
    (1,2,3,3,2,1),
    (1,2,2,5,4,3),
    (10,1,2,1,5,1)
]
purchase_columns = ('time','id_customer','id_product','count')


columns = ', '.join(product_columns)
placeholders = ', '.join('?' * len(product_columns))
sql_insert_product = 'INSERT INTO Product ({}) VALUES ({})'.format(columns, placeholders)
#print(sql_insert_product)
columns = ', '.join(customer_columns)
placeholders = ', '.join('?' * len(customer_columns))
sql_insert_customer = 'INSERT INTO Customer ({}) VALUES ({})'.format(columns, placeholders)
columns = ', '.join(purchase_columns)
placeholders = ', '.join('?' * len(purchase_columns))
sql_insert_purchase = 'INSERT INTO Purchase ({}) VALUES ({})'.format(columns, placeholders)

curs.execute(drop_table_customer)
curs.execute(drop_table_product)
curs.execute(drop_table_purchase)
curs.execute(create_table_customer)
curs.execute(create_table_product)
curs.execute(create_table_purchase)
conn.commit()

#for row in curs.execute(list_table_name):
#    print(row)
for i in range(len(values_product[0])):
    curs.execute(sql_insert_product, [values_product[0][i], values_product[1][i]])
for i in range(len(values_customers[0])):
    curs.execute(sql_insert_customer, [values_customers[0][i],values_customers[1][i]])
for i in range(len(values_purchase[0])):
    curs.execute(sql_insert_purchase, [values_purchase[0][i],values_purchase[1][i],values_purchase[2][i],values_purchase[3][i]])
conn.commit()

#закончили заполнение таблиц
reqest_1 = '''
SELECT * FROM Purchase WHERE count >3
'''
reqest_2 = '''
SELECT * FROM Purchase WHERE time LIKE '%-01-%'
'''
reqest_3 = '''
SELECT * FROM Customer WHERE adress LIKE '%Екатеринбург%'
'''
PrintTable(curs,reqest_3)
conn.close()