from PIL import Image
import threading
import queue
import os

def inputer():
    while True:
        a = input().split()  #разбиваем входящую строку на файл, код задания и принадлежность
        if a == 'exit'.split(): #Если на входе exit то закидываем None-ы по количеству рабочих потоков
            for i in range(n):
                qinput.put(None)
            break
        else:
            qinput.put(a)

def worker():
    while True:
        task = qinput.get()
        if task == None:  #Если на входе None то завершаем поток и закидываем на выход None
            qoutput.put(None)
            break
        else:
            img = Image.open(task[0])
            width = img.size[0]
            height = img.size[1]
            pix = img.load()   #выгружаем значения пикселей
            if task[1] == '1':
                result = WhtBlck(pix, width, height)
            elif task[1] == '2':
                result = Invrs(pix, width, height)
            qoutput.put((result, task[2])) #возвращаем список пикселей и принадлежность
            img.close()

def outputer():
    threads = 0  #счетчик завершенных worker-ов
    y = 1    #счетчики для нумерации результатов
    x = 1
    z = 1
    unk = 1
    while True:
        result = qoutput.get()
        if result == None:
            threads +=1
        elif result[1] == 'вася':
            if not os.path.exists('Vasya'): # Если папка Васи не существует то создаем её
                os.mkdir('Vasya')
            result[0].save('Vasya/result{}.jpg'.format(str(y))) #сохраняем изображения
            y += 1
        elif result[1] == 'петя':
            if not os.path.exists('Petya'):
                os.mkdir('Petya')
            result[0].save('Petya/result{}.jpg'.format(str(x)))
            x += 1
        elif result[1] == 'варя':
            if not os.path.exists('Varya'):
                os.mkdir('Varya')
            result[0].save('Varya/result{}.jpg'.format(str(z)))
            z += 1
        else:
            if not os.path.exists('Unknown'):
                os.mkdir('Unknown')
            result[0].save('Unknown/result{}.jpg'.format(str(unk)))
            unk += 1
        if threads == n:  #после завершения всез worker-ов завершаем поток
            break

def WhtBlck(image,w,h):
    NewImage = Image.new('RGB', (w, h))
    for i in range(w):
        for j in range(h):
            a = image[i, j][0]
            b = image[i, j][1]
            c = image[i, j][2]
            mid = (a + b + c) // 3
            NewImage.putpixel((i, j), (mid, mid, mid))
    return NewImage

def Invrs(image,w,h):
    NewImage = Image.new('RGB', (w, h))
    for i in range(w):
        for j in range(h):
            a = image[i, j][0]
            b = image[i, j][1]
            c = image[i, j][2]
            NewImage.putpixel((i, j), (255 - a, 255 - b, 255 - c))
    return NewImage
n = 3
qinput = queue.Queue()  #очередь входа из inputer в worker
qoutput = queue.Queue()  #очередь выхода из worker в outputer
inputer1 = threading.Thread(target=inputer)   #создаем потоки
outputer1 = threading.Thread(target = outputer)
Threads = []
for i in range(n):
    thread = threading.Thread(target=worker)
    Threads.append(thread)

inputer1.start()  #запускаем потоки
outputer1.start()
for i in Threads:
    i.start()

inputer1.join()   #ждем завершения потоков
outputer1.join()
for i in Threads:
    i.join()

#образец запуска:
#war1.jpg 1 вася
#war2.jpg 2 варя
#war1.jpg 2 петя
#war2.jpg 1 вася

