from tkinter import Tk, Button, StringVar, OptionMenu, Text, WORD
from tkinter.filedialog import askdirectory
from collections import defaultdict
import os

root = Tk()

dict_settings = {
    'directory': None,
}
result_dict = defaultdict(list)

def Start(event):
    if dict_settings['directory'] is not None:
        files = get_list_files(dict_settings['directory'])
        for file in files:
            result_dict[os.path.getsize(file)].append(file)
        for key, value in result_dict.items():
            if len(value) > 1:


def Askopen(event):
    dict_settings['directory'] = askdirectory()

def get_list_files(folder):
    files = []
    ext = extention_variable.get()
    for root, dirnames, filenames in os.walk(folder):
        for filename in filenames:
            if filename[-len(ext):] == ext:
                files.append(os.path.join(root, filename))
    return files

extentions = ('.pdf', '.djvu')
extention_variable = StringVar(root)
extention_variable.set(extentions[0])

params_btn = {
    'width': 30,
    'height': 5,
    'bg': 'white',
    'fg': 'black',
}

extention_menu = OptionMenu(root, extention_variable, *extentions)

btn_select_folder = Button(root,
             text="Выберете папку",
             **params_btn)
btn_start = Button(root,
                   text ='искать',
                   **params_btn)

result_text = Text(root, width=40,
                   font="Verdana 12",
                   wrap=WORD)

btn_select_folder.bind("<Button-1>", Askopen)
btn_start.bind('<Button-1>',Start)
btn_select_folder.pack()
extention_menu.pack()
btn_start.pack()
root.mainloop()