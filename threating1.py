import threading
import time

def writer(x):
    for i in range(10):
        time.sleep(1)
        if x ==1:
            s = 0
            for j in range(10**7):
                s +=1
        print('Поток {0}'.format(x), i)

t1 = threading.Thread(target=writer, args =(1,))
t2 = threading.Thread(target=writer, args =(2,))

t1.start()
t2.start()

t1.join()
t2.join()